﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SpeechBubble : MonoBehaviour
{
    //A Boolean to used for when a user is typing
    bool isTypingMessage;
    //The Text object, for message, it is set to false as a default
    Text message;
    //This object is the speech bubble, for the mean time it is just a quad
    GameObject speechBubble;
    //Third person controller, the variable will be disabled, when the user is typing
    public GameObject thirdPersonController;
    // Use this for initialization
    void Start ()
    {
        isTypingMessage = false;
        message = gameObject.GetComponent<Text>();
        message.enabled = false;
        speechBubble = gameObject.transform.GetChild(0).gameObject;
        speechBubble.SetActive(false);
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Loop through each character inputting
        foreach (char character in Input.inputString)
        {
            //If enter is pressed, and not typing a message
            if (Input.GetKeyDown(KeyCode.Return) && !isTypingMessage)
            {//set the typing message bool to true
                isTypingMessage = true;
            }//else when bool is true, and the return key is hit again
            else if (isTypingMessage && Input.GetKeyDown(KeyCode.Return))
            {//set the typing message to false
                isTypingMessage = false;
                //TODO some fancy lerp of the speech bubble

                //Set the text for the message to empty
                message.text = "";
                //break out of the loop
                break;
            }
            //if the bool is true, start taking down the characters, and display them as the message!
            if (isTypingMessage)
            {
                message.text += character;
            }
        }
        //For when the bool isTypingMessage set to true, the third person controller not enabled 
        thirdPersonController.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().enabled = !isTypingMessage;
        //enabled/disable the Text object message, based on the isTypingMessage bool
        message.enabled = isTypingMessage;
        //Active/Deactive the speech bubble object based on the isTypingMessage bool
        speechBubble.SetActive(isTypingMessage);
    }
}
